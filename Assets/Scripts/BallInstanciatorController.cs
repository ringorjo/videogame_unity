﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallInstanciatorController : MonoBehaviour
{
    public GameObject Prefab;
    public Transform[] TriggerBall;
    public Transform Center;
    public int BallTimeInterval;
    public float Force=10;
    private int RandomInterval;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TriggerBallController());
    }

    IEnumerator TriggerBallController()
    {
        while (true)
        {
            yield return new WaitForSeconds(BallTimeInterval);
            RandomInterval = Random.Range(0, TriggerBall.Length);
            GameObject Ball = Instantiate(Prefab);
            Ball.transform.position = TriggerBall[RandomInterval].position;
            Ball.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            Vector3 heading = Center.position - Ball.transform.position;
            float distance = heading.magnitude;
            Vector3 direction = heading / distance;
            Ball.GetComponent<Rigidbody>().AddForce(direction * Force);

            
            
        }
    }
}
