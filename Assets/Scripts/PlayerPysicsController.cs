﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPysicsController : MonoBehaviour
{
    public Transform Center;
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Ball")
        {
            GameObject Ball = collision.gameObject;
            Vector3 heading = Center.position - Ball.transform.position;
            float distance = heading.magnitude;
            Vector3 direction = heading / distance;
            Ball.GetComponent<Rigidbody>().AddForce(direction * 60);


        }
    }
}
