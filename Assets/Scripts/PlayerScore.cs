﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerScore : MonoBehaviour
{

    private int IntialScore;
    public GameObject Player;
    public string PlayerName;
    public int ScorePlayer;
    public Text ScoreUi;

    private void Start()
    {
        IntialScore = ScoreController.instance.SetIntialScoreToPlayers();
        ScorePlayer = IntialScore;
        ScoreUi.text = PlayerName + ": " + ScorePlayer;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball")
        {
            ScorePlayer = ScorePlayer - ScoreController.instance.LoseLife();
            ScoreUi.text = PlayerName + ": " + ScorePlayer;
            Destroy(other, 2);
            ValideScore();
        }
          
    }

    public void ValideScore()
    {
        if(ScorePlayer<=0)
        {
            Debug.Log("Perdiste: "+PlayerName);
            Destroy(Player);
            transform.GetComponent<Collider>().isTrigger = false;
        }
    }



}
