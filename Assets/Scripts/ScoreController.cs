﻿using UnityEngine;

public class ScoreController : MonoBehaviour
{
    public int IntialScore;
    public static ScoreController instance = null;
    private void Awake()
    {

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    public int SetIntialScoreToPlayers()
    {
        return IntialScore;
    }

    public int LoseLife()
    {
        return 1;
    }

    
}

